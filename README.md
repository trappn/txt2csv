# txt2csv

OUTPUT CONVERTER FOR LINKAM DSC600 STAGE

A little script that makes .csv-format written by LINK ((c) Linkam Scientific)
more usable. The program comes with a simple graphical file dialog.
This was written for a Temperature/DSC/Video stage.

The input file is preserved, output will be named <input>_converted.csv

What it does:
- get rid of everything in the header
- drop every data column except Index, Time, Temp, DSC & TASC
- more meaningful/readable column headers
- get rid of 1000-separators
- reformat all numbers to 3 decimal places
- insert semi-colons as separators (= Excel legacy)
- output should be usable "as-is" in Excel
- handles datasets regardless if data points were compressed or not
- Origin should recognize format as Excel standard
- csv2plot.py for quick-and-dirty plotting of the converted .csv file (see below) - for now you'll need a Python3 installation with matplotlib and tkinter (I'll create an installer at some point)
- csv2classic.py for quick-and-dirty plotting of a classic T *vs.* heat flow plot, using the converted .csv file. The script will try to identify heating and cooling cycles and draw the curves in warm and cold colors, accordingly (multiple colors will be cycled, hold=grey). This needs more testing.
- csv2plot.py and csv2classic.py are aware of TASC data (available or not) and will process/reformat the plot&data accordingly

![alt text1][plot1]

[plot1]: img/plot1.png "DSC-only plot"

![alt text2][plot2]

[plot2]: img/plot2.png "DSC&TASC plot"

![alt text3][plot3]

[plot3]: img/plot3.png "Classic T vs. heat flow plot"