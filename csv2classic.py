import matplotlib.pyplot as plt
from cycler import cycler
import csv
import os
import tkinter as tk
from tkinter import filedialog

###### GRAPH COLORS: ########################################################################
heat_cycler = (cycler(color=['r', 'y', 'darkorange', 'darkred', 'm', 'pink']))
cool_cycler = (cycler(color=['b', 'g', 'c', 'cornflowerblue', 'midnightblue', 'lightgreen']))
hold_cycler = (cycler(color=['grey']))
#############################################################################################

###### Threshold for gradient change detection: #############################################
threshold = 0.00025
#############################################################################################

# Input file handling / File dialog:
root = tk.Tk()
root.withdraw()
inp_file = filedialog.askopenfilename()
print('input file: '+inp_file)

T = []
DSC = []
error = False

with open(inp_file,'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    ncol=len(next(plots))       # Read first line and count columns
                                # ncol=6: uncompressed, 7: compressed data points
    csvfile.seek(0)             # go back to beginning of file
    if ncol==6:
        print('6 data columns found (uncompressed data). Drawing plot...')
        for row in plots:
            try:
                T.append(float(row[2]))     # temp
                DSC.append(float(row[3]))   # DSC
            except:
                pass
    else:
        print('7 data columns found (compressed data). Drawing plot...')
        for row in plots:
            try:
                T.append(float(row[3]))     # temp
                DSC.append(float(row[4]))   # DSC
            except:
                pass

if error:
    print('Faulty input file, quitting.')
else:
    Tsplit = []
    previousy = T[0]   # set up comparators
    previousx = 0
    previousgrad = 0
    v = 0
    ci = 0             # cycle index for setting up colors
    direction = 'hold'
    flip = False
    for i, t in enumerate(T):
        if v==20:
            v = 0
            grad = ((t-previousy)/(i-previousx))      # re-determine gradient over last 20 points
            # now detect sign change (& its significance, threshold set by variable):
            if abs(grad) > threshold and abs(grad + previousgrad) != abs(grad) + abs(previousgrad):
                ci=ci+1
            if abs(grad) > threshold and grad < 0:
                direction = 'cool'
            elif abs(grad) > threshold and grad > 0:
                direction = 'heat'
            else:
                direction = 'hold'
            previousgrad = grad   
            previousx = i
            previousy = t
        else:
            v = v+1
        Tsplit.append([ci,t,DSC[i],direction])
    print('Number of detected direction changes: '+str(ci))
    #print(Tsplit)  # makes IDLE crash, test in terminal      
    fig, ax1 = plt.subplots(num='DSC Plot')
    ax1.set_xlabel('T/°C')
    ax1.set_ylabel('DSC')

    plotx = []
    ploty = []
    cilast = 0
    for i in Tsplit:
        plotx.append(i[1])
        ploty.append(i[2])
        #print(i[3])
        if i == Tsplit[-1]:                                         # if end of list is reached without direction change, just plot
            if i[3] == 'cool':
                ax1.set_prop_cycle(cool_cycler)
            elif i[3] == 'heat':
                ax1.set_prop_cycle(heat_cycler)
            else:
                ax1.set_prop_cycle(hold_cycler)
            ax1.plot(plotx, ploty)
        elif i[0] != cilast:                                         # when direction indicator flips, plot everything we
            if i[3] == 'cool':                                       # collected so far, then flush the list&restart with next segment               
                ax1.set_prop_cycle(cool_cycler)
            elif i[3] == 'heat':
                ax1.set_prop_cycle(heat_cycler)
            else:
                ax1.set_prop_cycle(hold_cycler)
            ax1.plot(plotx, ploty)                                   
            del plotx[:]
            del ploty[:]
            cilast = i[0]
    ax1.tick_params(axis='y', direction='in')
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.title('DSC plot: '+os.path.basename(inp_file))
    plt.draw()
    plt.show()
