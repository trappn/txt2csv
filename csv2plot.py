import matplotlib.pyplot as plt
import csv
import os
import tkinter as tk
from tkinter import filedialog

###### GRAPH COLORS: ###################################################
T_col = 'tab:red'
DSC_col = 'tab:orange'
TASC_col = 'tab:blue'
########################################################################

# Input file handling / File dialog:
root = tk.Tk()
root.withdraw()
inp_file = filedialog.askopenfilename()
print('input file: '+inp_file)

t = []
T = []
DSC = []
tTASC = [] 
TASC = []
error = False

with open(inp_file,'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    ncol=len(next(plots))       # Read first line and count columns
                                # ncol=6: uncompressed, 7: compressed data points
    csvfile.seek(0)             # go back to beginning of file
    if ncol==6:
        print('6 data columns found (uncompressed data). Drawing plot...')
        for row in plots:
            try:
                t.append(float(row[1]))     # time
                T.append(float(row[2]))     # temp
                DSC.append(float(row[3]))   # DSC
                tTASC.append(float(row[4])) # TASC x
                TASC.append(float(row[5]))  # TASC y
            except:
                pass
    else:
        print('7 data columns found (compressed data). Drawing plot...')
        for row in plots:
            try:
                t.append(float(row[2]))     # time
                T.append(float(row[3]))     # temp
                DSC.append(float(row[4]))   # DSC
                tTASC.append(float(row[4])) # TASC x
                TASC.append(float(row[5]))  # TASC y
            except:
                pass

if error:
    print('Faulty input file, quitting.')
else:
    fig, ax1 = plt.subplots(num='DSC Plot')
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    
    ax1.set_xlabel('t/s')
    ax1.set_ylabel('T/°', color=T_col)
    ax1.plot(t, T, color=T_col)
    ax1.tick_params(axis='y', direction='in', labelcolor=T_col)
    
    ax2.set_ylabel('DSC', color=DSC_col)  # already handled the x-label with ax1
    ax2.plot(t, DSC, color=DSC_col)
    ax2.tick_params(axis='y', direction='in', labelcolor=DSC_col)

    if not TASC:
        print('No TASC data found, plotting only DSC.')
    else:
        print('TASC data found.')
        ax3 = ax1.twinx()  # instantiate a third axes that shares the same x-axis
        ax3.set_ylabel('TASC', color=TASC_col)
        ax3.plot(tTASC, TASC, color=TASC_col)
        ax3.tick_params(axis='y', direction='in', labelcolor=TASC_col)
        fig.subplots_adjust(right=0.75)   # Make some space on the right side for the extra y-axis
        ax3.spines['right'].set_position(('axes', 1.18))   # Move the TASC y-axis right by 18%
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    if not TASC:
        plt.title('DSC plot: '+os.path.basename(inp_file))
    else:
        plt.title('DSC/TASC plot: '+os.path.basename(inp_file))
    plt.draw()
    plt.show()
