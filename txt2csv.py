import csv
import re
import os
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

inp_file = filedialog.askopenfilename()
csv_file = os.path.splitext(inp_file)[0]+'_converted.csv'

print('input file: '+inp_file)
print('output file: '+csv_file)
 
with open(inp_file, "r") as file:
    if 'New Index' in file.read():
        included_cols = [0, 1, 2, 3, 11, 22, 23]
        column_header = ['newIndex','origIndex','t[s]','T[°C]','DSC','TASC_x[s]','TASC_y']
        nheaderlines = 107
    else:
        included_cols = [0, 1, 2, 10, 21, 22]
        column_header = ['Index','t[s]','T[°C]','DSC','TASC_x[s]','TASC_y']
        nheaderlines = 108
with open(inp_file, "r") as file:
    csv_reader_object = csv.reader(file, delimiter = '\t')
    [next(csv_reader_object, None) for item in range(nheaderlines)]
    with open(csv_file, "w", newline='') as outfile:
        writer = csv.writer(outfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(column_header)
        for row in csv_reader_object:
            try:
                filtered_content = list(row[i] for i in included_cols)
                filtered_content = [re.sub('[^0-9\.\-]', '', s) for s in filtered_content]
                for s in range(len(filtered_content)):
                    if s>0:
                        filtered_content[s] = "{:.3f}".format(float(filtered_content[s]))
            except:
                pass
            #print(filtered_content)
            writer.writerow(filtered_content)

                
